import React from 'react';
import './btnComponent.scss';

const BtnComponent = (props) => {
    return (
        <div className='stopwatch'>
            <button className="stopwatch-btn stopwatch-btn-start" onClick={props.start}>Start</button>
            <button className="stopwatch-btn stopwatch-btn-stop" onClick={props.stop}>Stop</button>
            <button className="stopwatch-btn stopwatch-btn-reset" onClick={props.reset}>Reset</button>
            <button className="stopwatch-btn stopwatch-btn-wait" onClick={props.wait}>Wait</button>
        </div>
    );
};

export default BtnComponent;