import React from 'react';

import './displayComponent.scss';

const DisplayComponent = (props) => {
    const h = () => {
        if(props.time.h === 0){
            return '';
        }else {
            return <span>{(props.time.h >= 10)? props.time.h : "0"+ props.time.h}</span>;
        }
    }
    return (
        <div className='timer'>
            <h1 className='timer-heading'>Timer</h1>
            <span className='timer-number'>{(props.time.h >= 10)? props.time.h : "0"+ props.time.h}</span>&nbsp;:&nbsp;
            <span className='timer-number'>{(props.time.m >= 10)? props.time.m : "0"+ props.time.m}</span>&nbsp;:&nbsp;
            <span className='timer-number'>{(props.time.s >= 10)? props.time.s : "0"+ props.time.s}</span>
        </div>
    );
};

export default DisplayComponent;