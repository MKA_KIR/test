import React, {useState} from 'react';
import DisplayComponent from './client/timer/components/displayComponent';
import BtnComponent from './client/timer/components/btnComponent';

import './App.css';

function App() {

  const [time, setTime] = useState({s:0, m:0, h:0});
  const [interv, setInterv] = useState();

  const start = () => {
    run();
    setInterv(setInterval (run, 10));
  };

  let  updatedS = time.s, updatedM = time.m, updatedH = time.h;

  const run = () => {
    if(updatedH === 60){
      updatedM++;
      updatedH = 0;
    }
    if(updatedM === 60){
      updatedH++;
      updatedM = 0;
    }
    if(updatedS === 60){
      updatedM++;
      updatedS = 0;
    }

    updatedS++;
    return setTime({s:updatedS, m:updatedM, h:updatedH});
  };

  const stop = () => {
    clearInterval(interv);
    setTime({s:0, m:0, h:0});
  };

  const reset = () => {
    clearInterval(interv);
    setTime({s:0, m:0, h:0});
  };

  const wait = () => stop();
  return (
    <div className="App">
      <div>
        <DisplayComponent time={time}/>
        <BtnComponent wait={wait} reset={reset} stop={stop} start={start}/>
      </div>
    </div>
  );
}

export default App;
